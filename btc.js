const request = require('request');
var async = require('async');
var express = require('express');
var app = express();
var path = require('path');
var pub = __dirname + "/public/";

app.get('/idx', function(req, res) {
  res.sendFile(path.join(pub + "Btc.html"));
});

app.get('/chart', function(req, res) {
  async.parallel([
    function(callback) {
      var url = "https://api.bitfinex.com/v1/pubticker/btcusd";
      request(url, function(err, response, body) {
        var price = -1.0;
        if (!err) {
          var js = JSON.parse(body);
          price = js['mid'];
        }
        callback(false, '"bitfinex": ' + price);
      });
    },
    function(callback) {
      var url = "https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT";
      request(url, function(err, response, body) {
        var price = -1.0;
        if (!err) {
          var js = JSON.parse(body);
          price = js['price'];
        }
        callback(false, '"binance": ' + price);
      });
    },
    function(callback) {
      var url = "https://poloniex.com/public?command=returnTicker";
      request(url, function(err, response, body) {
        var price = -1.0;
        if (!err) {
          var js = JSON.parse(body);
          price = js.USDT_BTC.last;
        }
        callback(false, '"polo": ' + price);
      });
    },
  ],
  function(err, results) {
    var str = "{" + results[0] + ", " + results[1] + ", " + results[2] + "}";
    console.log(str);
    res.setHeader('Content-Type', 'application/json');
    res.send(str);
  }
  );
});

app.use('/', express.static(pub));

app.listen(3000);
